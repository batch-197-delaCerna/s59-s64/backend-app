const Order = require('../models/Order')
const User = require('../models/User')
const Product = require('../models/Product')
const auth = require('../auth')

async function addOrder(req, res) {
    const userData = auth.decode(req.headers.authorization);
    if (userData.isAdmin) {
        return res.send(true)
    }

    try {
        const product = await Product.findById(req.body.productId);
        let newOrder = new Order({
            userId: userData.id,
            products: [req.body],
            totalAmount: product.price * req.body.quantity
        })
        let order = await newOrder.save()
        if (order) {
            return res.send(true)
        }
        return res.send(false)
    } catch (error) {
        return res.send(error)
    }
};

async function getOrder(req, res) {
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)
    if (userData.isAdmin) {
        return res.send(true)
    }
    try {
        const order = await Order.findById(req.params.orderId)
        if (order) {
            return res.send(order)
        }
        const userOrder = await Order.find({ _id: req.params.orderId, userId: user.id })
        if (userOrder.length === 0) {
            return res.send(false)
        }
        res.send(order)

    } catch (error) {
        return res.send(error)
    }
};

async function getAllOrders(req, res) {
    const userData = auth.decode(req.headers.authorization)

    try {
        if (!userData.isAdmin) {
            return res.send(false)
        }
        const orders = await Order.find({})
        if (orders) {
            return res.send(orders)
        }
        const userOrders = await Order.find({ userId: user.id })
        res.send(userOrders)
    } catch (error) {
        console.log(error)
        return res.send(error)
    }
};

module.exports = {
    addOrder: addOrder,
    getOrder: getOrder,
    getAllOrders: getAllOrders
}