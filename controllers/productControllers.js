const Product = require('../models/Product')
const auth = require('../auth')


async function addProduct(req, res) {
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)
    if (!userData.isAdmin) {
        return res.send("Only Admin can Add new Products")
    }
    try {

        let newProduct = new Product({
            name: req.body.name,
            price: req.body.price,
            description: req.body.description,
            // image: req.body.image
        })
        const product = await newProduct.save()
        if (product) {
            return res.send(true)
        }
        return res.send(false)
    } catch (error) {
        console.log(error)
        return res.send(error)
    }
};

async function getAllProducts(req, res) {

    try {
        const product = await Product.find({})

        if (product) {
            return res.send(product)
        }
        return res.send(false)
    } catch (error) {
        console.log(error)
        return res.send(error)
    }
};

async function getActiveProducts(req, res) {
    try {
        const product = await Product.find({ isActive: true })

        if (product && product.length > 0) {
            return res.send(product)
        }
        return res.send(false)
    } catch (error) {
        console.log(error)
        return res.send(error)
    }
};

async function getSingleProduct(req, res) {
    try {
        const product = await Product.findById(req.params.productId)
        if (product) {
            return res.send(product)
        }
        return res.send(false)
    } catch (error) {
        console.log(error)
        return res.send(error)
    }
};

async function updateTheProduct(req, res) {
    const userData = auth.decode(req.headers.authorization)

    try {
        const updatedProduct = {
            name: req.body.name,
            price: req.body.price,
            description: req.body.description
        }

        if (!userData.isAdmin) {
            return true
        }
        const result = await Product.findByIdAndUpdate(req.params.productId, updatedProduct)

        if (result) {
            return res.send(updatedProduct)
        }
        return res.send(false)
    } catch (error) {
        console.log(error)
        return res.send(error)
    }
};

async function archiveProduct(req, res) {
    const userData = auth.decode(req.headers.authorization)

    try {
        if (!userData.isAdmin) {
            return true
        }
        const result = await Product.findByIdAndUpdate(req.params.productId, { isActive: req.body.isActive })

        if (result) {
            return res.send(true)
        }
        return res.send(false)
    } catch (error) {
        console.log(error)
        return res.send(error)
    }
};

async function activeProduct(req, res) {
    const userData = auth.decode(req.headers.authorization)

    try {
        if (!userData.isAdmin) {
            return true
        }
        const result = await Product.findByIdAndUpdate(req.params.productId, { isActive: true });


        if (result) {
            return res.send(true)
        }
        res.send(false);
    } catch (error) {
        console.log(error)
        return res.send(error)
    }
};



module.exports = {
    addProduct: addProduct,
    getAllProducts: getAllProducts,
    getActiveProducts: getActiveProducts,
    getSingleProduct: getSingleProduct,
    updateTheProduct: updateTheProduct,
    archiveProduct: archiveProduct,
    activeProduct: activeProduct,
}