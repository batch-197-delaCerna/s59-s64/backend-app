const User = require('../models/User')
const bcrypt = require('bcrypt')
const auth = require('../auth')


async function checkEmailExists(req, res) {
    try {
        const result = await User.find({ email: req.body.email });
        if (result.length > 0) {
            return res.send(true);
        }
        return res.send(false);
    } catch (error) {
        console.log(error);
        return res.send(error);
    }
}


async function registerUser(req, res) {
    const hashedPassword = bcrypt.hashSync(req.body.password, 10)

    try {
        const newUser = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: hashedPassword,
            mobileNo: req.body.mobileNo
        })
        const user = await User.findOne({ email: req.body.email })
        if (user !== null) {
            return res.send(false)
        }
        const result = await newUser.save()
        if (result) {
            return res.send(true)
        }
        return res.send(false)
    } catch (error) {
        console.log(error)
        return res.send(error)
    }
};

async function loginUser(req, res) {
    try {
        const user = await User.findOne({ email: req.body.email })
        if (user == null) {
            return res.send(true);
        }
        const isPasswordCorrect = bcrypt.compareSync(
            req.body.password,
            user.password
        )
        if (isPasswordCorrect) {
            return res.send({ access: auth.createAccessToken(user) })
        }
        return res.send(false)
    } catch (error) {
        console.log(error)
        return res.send(error)
    }
};

async function getProfile(req, res) {
    const userData = auth.decode(req.headers.authorization)
    console.log(userData.id)
    try {
        const user = await User.findById(userData.id, { password: 0 })
        console.log(user);
        if (user) {
            return res.send(user)
        }
        return res.send("User ID not found")
    } catch (error) {
        console.log(error)
        res.send(error)
    }
};

async function updateUser(req, res) {
    const userData = auth.decode(req.headers.authorization)
    try {
        if (!userData.isAdmin) {
            return res.send("User is not Authorized")
        }
        const result = await User.findByIdAndUpdate(req.body.userId, { isAdmin: req.body.isAdmin })

        if (result) {
            return res.send(true)
        }
        return res.send(false)
    } catch (error) {
        console.log(error)
        return res.send(error)
    }
};

async function changePassword(req, res) {
    const userData = auth.decode(req.headers.authorization)
    try {
        if (req.body.newPassword !== req.body.confirmNewPassword) {
            return res.send('Password Incorrect')
        }
        const updatePassword = await User.findById(userData.id)
        const isPasswordCorrect = bcrypt.compareSync(
            req.body.password,
            updatePassword.password
        )

        if (!isPasswordCorrect) {
            return res.send('password incorrect')
        }

        const hashedPassword = bcrypt.hashSync(req.body.newPassword, 10)
        updatePassword.password = hashedPassword
        const updatedPassword = await updatePassword.save()
        return res.send({ access: auth.createAccessToken(updatedPassword) })
    } catch (error) {
        return res.send(error)
    }
}


module.exports = {
    checkEmailExists: checkEmailExists,
    registerUser: registerUser,
    loginUser: loginUser,
    getProfile: getProfile,
    updateUser: updateUser,
    changePassword: changePassword
}