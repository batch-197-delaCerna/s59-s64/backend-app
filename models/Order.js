const mongoose = require('mongoose')

const orderSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    products: [{
        productId: {
            type: String,
            required: true
        },
        quantity: {
            type: Number,
            default: 1
        }
    }],
    totalAmount: {
        type: Number,
        default: 0
    },
    puchasedOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('Order', orderSchema)