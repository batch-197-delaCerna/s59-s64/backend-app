const mongoose = require('mongoose');

mongoose.connect(`mongodb+srv://aronndc:admin123@zuitt-batch-197.lzwxz7u.mongodb.net/Capstone3?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const dbConnect = mongoose.connection;

dbConnect.on('error', () => console.error('Connection Error'));
dbConnect.once('open', () => console.log('Connected to MongoDB!'));


module.exports = dbConnect;